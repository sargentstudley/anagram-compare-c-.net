﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HelloWorldAnanagram;

namespace AnagramUnitTests
{
    [TestClass]
    public class AnagramTests
    {
       
        [TestMethod]
        public void PositiveAnagramTest()
        {
            string firstword = "Amleth";
            string secondword = "Hamlet";

            bool result = AnanagramCompare.isAnanagram(firstword, secondword);
            Assert.IsTrue(result, "The result was not true as expected.");
        }

        [TestMethod]
        public void NegativeAnagramTest()
        {
            string firstword = "Matt";
            string secondword = "Studley";

            bool result = AnanagramCompare.isAnanagram(firstword, secondword);
            Assert.IsFalse(result, "The result is not false as expected.");
        }
    }
}
