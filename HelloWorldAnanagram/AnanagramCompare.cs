﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldAnanagram
{
   public static class AnanagramCompare
    {
        public static bool isAnanagram(string firstWord, string secondWord)
        {
            Dictionary<char,short> lettersInWord = new Dictionary<char,short>();

            foreach(char l in firstWord.ToLower())
            {
                if (!lettersInWord.ContainsKey(l))
                {
                    lettersInWord.Add(l, 1);
                }
                else
                {
                    lettersInWord[l]++;
                }
            }

            foreach (char l in secondWord.ToLower())
            {
                if (!lettersInWord.ContainsKey(l))
                {
                    return false; //We encountered a letter we don't have yet, so these words can't be an ananagram.
                }
                else
                {
                    lettersInWord[l]--;
                }
            }

            var lettersLeft = lettersInWord.Where(l => l.Value != 0);

            if (lettersLeft.Count() != 0) return false; //We found dictionary values where the number of letters left isn't 0. 

            return true;
        }

      
    }
}
