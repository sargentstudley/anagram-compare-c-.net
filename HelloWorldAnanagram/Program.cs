﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldAnanagram
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Let's comapre two words and see if they are ananagrams.");
            System.Console.WriteLine("Please enter the first word:");
            string firstword = System.Console.ReadLine();

            System.Console.WriteLine("Please enter the second word:");
            string secondword = System.Console.ReadLine();

            bool result = AnanagramCompare.isAnanagram(firstword, secondword);

            System.Console.WriteLine(string.Format("Are these an anagram? Survey says: {0}", result.ToString()));

            System.Console.WriteLine("Press any key to exit.");

            System.Console.ReadLine();

        }
    }
}
